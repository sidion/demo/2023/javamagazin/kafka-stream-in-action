package de.idion.kafka;

import de.idion.kafka.topologies.ExampleTopology;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;
import java.util.Properties;
import java.util.UUID;

@SpringBootApplication
public class StreamingApplication implements CommandLineRunner {

    private final List<ExampleTopology> topologies;

    public StreamingApplication(List<ExampleTopology> topologies) {
        this.topologies = topologies;
    }

    public static void main(String[] args) {
        SpringApplication.run(StreamingApplication.class, args);

    }


    @Override
    public void run(String... args) throws Exception {
        final Properties streamsConfiguration = new Properties();
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, "streaming-example-" + UUID.randomUUID().toString());
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        streamsConfiguration.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        streamsConfiguration.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, 100);

        // build topology
        final StreamsBuilder builder = new StreamsBuilder();
        topologies.forEach(t -> t.createTopology(builder));


        // start streaming
        final Topology streamingTopology = builder.build();
        final KafkaStreams kafkaStreams = new KafkaStreams(streamingTopology, streamsConfiguration);
        kafkaStreams.start();
    }
}
