package de.idion.kafka.eventProducer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.idion.kafka.events.ArtikelEvent;
import de.idion.kafka.events.BestellPositionEvent;
import de.idion.kafka.events.BestellungEvent;
import de.idion.kafka.events.KundeEvent;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class EventProducer {

    public static void main(String[] args) throws JsonProcessingException {
        new EventProducer().produceEvents();
    }

    public void produceEvents() throws JsonProcessingException {

        final ObjectMapper objectMapper = new ObjectMapper();

        final List<KundeEvent> kundeEvents = Arrays.asList(
                KundeEvent.builder().idx("k01").email("augustus@foobar.de").name("Augustus").build(),
                KundeEvent.builder().idx("k02").email("claudius@foobar.de").name("Claudius").build(),
                KundeEvent.builder().idx("k03").email("neo@foobar.de").name("Nero").build(),
                KundeEvent.builder().idx("k04").email("maximilian@foobar.de").name("Maximilian").build(),
                KundeEvent.builder().idx("k05").email("caesar@foobar.de").name("Caesar").build()
        );

        final List<ArtikelEvent> artikelEvents = Arrays.asList(
                ArtikelEvent.builder().idx("a01").artikelName("ASUS Chromebook C523NA-EJ0123").kategorie("Computer & Büro").preis(249).build(),
                ArtikelEvent.builder().idx("a02").artikelName("ACER Aspire 3").kategorie("Computer & Büro").preis(529).build(),
                ArtikelEvent.builder().idx("a03").artikelName("AMAZON Fire TV Stick Lite").kategorie("Computer & Büro").preis(29).build(),
                ArtikelEvent.builder().idx("a04").artikelName("SEAGATE IronWolf Festplatte").kategorie("Computer & Büro").preis(99).build(),
                ArtikelEvent.builder().idx("a05").artikelName("SAMSUNG Galaxy S20 FE").kategorie("Smartphones").preis(415).build(),
                ArtikelEvent.builder().idx("a06").artikelName("MOTOROLA MOTO G60S 6").kategorie("Smartphones").preis(239).build(),
                ArtikelEvent.builder().idx("a07").artikelName("NOKIA G50").kategorie("Smartphones").preis(189).build(),
                ArtikelEvent.builder().idx("a08").artikelName("SONY Cybershot").kategorie("Fotografie").preis(109).build(),
                ArtikelEvent.builder().idx("a09").artikelName("EASYPIX KiddyPix Galaxy").kategorie("Fotografie").preis(49).build(),
                ArtikelEvent.builder().idx("a10").artikelName("INF Digitalkamera 1080P").kategorie("Fotografie").preis(70).build()
        );


        final Map<String, KundeEvent> kundenEventMap = kundeEvents.stream().collect(Collectors.toMap(KundeEvent::getIdx, Function.identity()));
        final Map<String, ArtikelEvent> articleEventMap = artikelEvents.stream().collect(Collectors.toMap(ArtikelEvent::getIdx, Function.identity()));

        final BestellungEvent b01 = BestellungEvent.builder().idx(UUID.randomUUID().toString()).bestellPositionen(Arrays.asList(
                BestellPositionEvent.builder().artikelEvent(articleEventMap.get("a01")).anzahlArtikel(1).idx(UUID.randomUUID().toString()).build()
        )).kundenIdx("k01").gesamtSumme(249).build();

        final BestellungEvent b02 = BestellungEvent.builder().idx(UUID.randomUUID().toString()).bestellPositionen(Arrays.asList(
                BestellPositionEvent.builder().artikelEvent(articleEventMap.get("a04")).anzahlArtikel(2).idx(UUID.randomUUID().toString()).build(),
                BestellPositionEvent.builder().artikelEvent(articleEventMap.get("a01")).anzahlArtikel(1).idx(UUID.randomUUID().toString()).build()
        )).kundenIdx("k02").gesamtSumme(447).build();

        final BestellungEvent b03 = BestellungEvent.builder().idx(UUID.randomUUID().toString()).bestellPositionen(Arrays.asList(
                BestellPositionEvent.builder().artikelEvent(articleEventMap.get("a03")).anzahlArtikel(1).idx(UUID.randomUUID().toString()).build(),
                BestellPositionEvent.builder().artikelEvent(articleEventMap.get("a09")).anzahlArtikel(1).idx(UUID.randomUUID().toString()).build(),
                BestellPositionEvent.builder().artikelEvent(articleEventMap.get("a06")).anzahlArtikel(1).idx(UUID.randomUUID().toString()).build()
        )).kundenIdx("k03").gesamtSumme(317).build();

        final BestellungEvent b04 = BestellungEvent.builder().idx(UUID.randomUUID().toString()).bestellPositionen(Arrays.asList(
                BestellPositionEvent.builder().artikelEvent(articleEventMap.get("a02")).anzahlArtikel(1).idx(UUID.randomUUID().toString()).build()
        )).kundenIdx("k04").gesamtSumme(529).build();

        final BestellungEvent b05 = BestellungEvent.builder().idx(UUID.randomUUID().toString()).bestellPositionen(Arrays.asList(
                BestellPositionEvent.builder().artikelEvent(articleEventMap.get("a03")).anzahlArtikel(1).idx(UUID.randomUUID().toString()).build()
        )).kundenIdx("k05").gesamtSumme(29).build();

        final BestellungEvent b06 = BestellungEvent.builder().idx(UUID.randomUUID().toString()).bestellPositionen(Arrays.asList(
                BestellPositionEvent.builder().artikelEvent(articleEventMap.get("a04")).anzahlArtikel(1).idx(UUID.randomUUID().toString()).build()
        )).kundenIdx("k01").gesamtSumme(99).build();

        final BestellungEvent b07 = BestellungEvent.builder().idx(UUID.randomUUID().toString()).bestellPositionen(Arrays.asList(
                BestellPositionEvent.builder().artikelEvent(articleEventMap.get("a05")).anzahlArtikel(1).idx(UUID.randomUUID().toString()).build()
        )).kundenIdx("k01").gesamtSumme(249).build();

        final BestellungEvent b08 = BestellungEvent.builder().idx(UUID.randomUUID().toString()).bestellPositionen(Arrays.asList(
                BestellPositionEvent.builder().artikelEvent(articleEventMap.get("a06")).anzahlArtikel(1).idx(UUID.randomUUID().toString()).build()
        )).kundenIdx("k01").gesamtSumme(239).build();

        final BestellungEvent b09 = BestellungEvent.builder().idx(UUID.randomUUID().toString()).bestellPositionen(Arrays.asList(
                BestellPositionEvent.builder().artikelEvent(articleEventMap.get("a07")).anzahlArtikel(1).idx(UUID.randomUUID().toString()).build()
        )).kundenIdx("k03").gesamtSumme(189).build();

        final BestellungEvent b10 = BestellungEvent.builder().idx(UUID.randomUUID().toString()).bestellPositionen(Arrays.asList(
                BestellPositionEvent.builder().artikelEvent(articleEventMap.get("a02")).anzahlArtikel(1).idx(UUID.randomUUID().toString()).build()
        )).kundenIdx("k03").gesamtSumme(529).build();

        final BestellungEvent b11 = BestellungEvent.builder().idx(UUID.randomUUID().toString()).bestellPositionen(Arrays.asList(
                BestellPositionEvent.builder().artikelEvent(articleEventMap.get("a04")).anzahlArtikel(1).idx(UUID.randomUUID().toString()).build()
        )).kundenIdx("k03").gesamtSumme(99).build();

        final BestellungEvent b12 = BestellungEvent.builder().idx(UUID.randomUUID().toString()).bestellPositionen(Arrays.asList(
                BestellPositionEvent.builder().artikelEvent(articleEventMap.get("a09")).anzahlArtikel(1).idx(UUID.randomUUID().toString()).build()
        )).kundenIdx("k03").gesamtSumme(49).build();

        final BestellungEvent b13 = BestellungEvent.builder().idx(UUID.randomUUID().toString()).bestellPositionen(Arrays.asList(
                BestellPositionEvent.builder().artikelEvent(articleEventMap.get("a03")).anzahlArtikel(1).idx(UUID.randomUUID().toString()).build()
        )).kundenIdx("k05").gesamtSumme(29).build();

        final BestellungEvent b14 = BestellungEvent.builder().idx(UUID.randomUUID().toString()).bestellPositionen(Arrays.asList(
                BestellPositionEvent.builder().artikelEvent(articleEventMap.get("a05")).anzahlArtikel(1).idx(UUID.randomUUID().toString()).build()
        )).kundenIdx("k04").gesamtSumme(415).build();

        final BestellungEvent b15 = BestellungEvent.builder().idx(UUID.randomUUID().toString()).bestellPositionen(Arrays.asList(
                BestellPositionEvent.builder().artikelEvent(articleEventMap.get("a07")).anzahlArtikel(1).idx(UUID.randomUUID().toString()).build()
        )).kundenIdx("k01").gesamtSumme(189).build();

        final BestellungEvent b16 = BestellungEvent.builder().idx(UUID.randomUUID().toString()).bestellPositionen(Arrays.asList(
                BestellPositionEvent.builder().artikelEvent(articleEventMap.get("a02")).anzahlArtikel(1).idx(UUID.randomUUID().toString()).build()
        )).kundenIdx("k05").gesamtSumme(529).build();

        final BestellungEvent b17 = BestellungEvent.builder().idx(UUID.randomUUID().toString()).bestellPositionen(Arrays.asList(
                BestellPositionEvent.builder().artikelEvent(articleEventMap.get("a01")).anzahlArtikel(1).idx(UUID.randomUUID().toString()).build()
        )).kundenIdx("k02").gesamtSumme(249).build();

        final BestellungEvent b18 = BestellungEvent.builder().idx(UUID.randomUUID().toString()).bestellPositionen(Arrays.asList(
                BestellPositionEvent.builder().artikelEvent(articleEventMap.get("a03")).anzahlArtikel(1).idx(UUID.randomUUID().toString()).build()
        )).kundenIdx("k04").gesamtSumme(29).build();

        final BestellungEvent b19 = BestellungEvent.builder().idx(UUID.randomUUID().toString()).bestellPositionen(Arrays.asList(
                BestellPositionEvent.builder().artikelEvent(articleEventMap.get("a04")).anzahlArtikel(1).idx(UUID.randomUUID().toString()).build()
        )).kundenIdx("k01").gesamtSumme(99).build();

        final BestellungEvent b20 = BestellungEvent.builder().idx(UUID.randomUUID().toString()).bestellPositionen(Arrays.asList(
                BestellPositionEvent.builder().artikelEvent(articleEventMap.get("a02")).anzahlArtikel(1).idx(UUID.randomUUID().toString()).build()
        )).kundenIdx("k03").gesamtSumme(529).build();

        sendToKafka(b01.getIdx(), objectMapper.writeValueAsString(b01), "bestellungen");
        sendToKafka(b02.getIdx(), objectMapper.writeValueAsString(b02), "bestellungen");
        sendToKafka(b03.getIdx(), objectMapper.writeValueAsString(b03), "bestellungen");
        sendToKafka(b04.getIdx(), objectMapper.writeValueAsString(b04), "bestellungen");
        sendToKafka(b05.getIdx(), objectMapper.writeValueAsString(b05), "bestellungen");
        sendToKafka(b06.getIdx(), objectMapper.writeValueAsString(b06), "bestellungen");
        sendToKafka(b07.getIdx(), objectMapper.writeValueAsString(b07), "bestellungen");
        sendToKafka(b08.getIdx(), objectMapper.writeValueAsString(b08), "bestellungen");
        sendToKafka(b09.getIdx(), objectMapper.writeValueAsString(b09), "bestellungen");
        sendToKafka(b10.getIdx(), objectMapper.writeValueAsString(b10), "bestellungen");
        sendToKafka(b11.getIdx(), objectMapper.writeValueAsString(b11), "bestellungen");
        sendToKafka(b12.getIdx(), objectMapper.writeValueAsString(b12), "bestellungen");
        sendToKafka(b13.getIdx(), objectMapper.writeValueAsString(b13), "bestellungen");
        sendToKafka(b14.getIdx(), objectMapper.writeValueAsString(b14), "bestellungen");
        sendToKafka(b15.getIdx(), objectMapper.writeValueAsString(b15), "bestellungen");
        sendToKafka(b16.getIdx(), objectMapper.writeValueAsString(b16), "bestellungen");
        sendToKafka(b17.getIdx(), objectMapper.writeValueAsString(b17), "bestellungen");
        sendToKafka(b18.getIdx(), objectMapper.writeValueAsString(b18), "bestellungen");
        sendToKafka(b19.getIdx(), objectMapper.writeValueAsString(b19), "bestellungen");
        sendToKafka(b20.getIdx(), objectMapper.writeValueAsString(b20), "bestellungen");

        kundeEvents.stream().forEach(ev -> {
            try {
                final String s = objectMapper.writeValueAsString(ev);
                sendToKafka(ev.getIdx(), s, "kunden");
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        });

        artikelEvents.stream().forEach(ev -> {
            try {
                final String s = objectMapper.writeValueAsString(ev);
                sendToKafka(ev.getIdx(), s, "artikel");
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        });
    }


    private void sendToKafka(String key, String value, String topic) {
        final Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("key.serializer", StringSerializer.class.getName());
        props.put("value.serializer", StringSerializer.class.getName());


        final KafkaProducer<String, String> producer = new KafkaProducer<>(props);
        final ProducerRecord<String, String> record = new ProducerRecord<>(topic, key, value);
        final Future<RecordMetadata> future = producer.send(record);
        try {
            future.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }
}
