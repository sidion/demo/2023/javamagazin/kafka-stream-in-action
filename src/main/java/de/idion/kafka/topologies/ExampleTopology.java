package de.idion.kafka.topologies;

import org.apache.kafka.streams.StreamsBuilder;

public interface ExampleTopology {

    void createTopology(final StreamsBuilder builder);
}
