package de.idion.kafka.topologies;

import de.idion.kafka.events.ArtikelEvent;
import de.idion.kafka.serde.SerdeFactory;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.*;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class BranchTopology implements ExampleTopology {


    @Override
    public void createTopology(StreamsBuilder builder) {

        final KStream<String, ArtikelEvent> artikelEvents =
                builder.stream("artikel", Consumed.with(Serdes.String(), SerdeFactory.serdeForArtikelEvent()));


        final Map<String, KStream<String, ArtikelEvent>> branchedArtikelEventMap = artikelEvents.split(Named.as("branch-"))
                .branch((key, val) -> val.getKategorie().equals("Computer & Büro"), Branched.as("computer"))
                .branch((key, val) -> val.getKategorie().equals("Smartphones"), Branched.as("smartphones"))
                .branch((key, val) -> val.getKategorie().equals("Fotografie"), Branched.as("fotografie"))
                .defaultBranch();

        branchedArtikelEventMap.get("branch-computer").to("artikel-computer", Produced.with(Serdes.String(), SerdeFactory.serdeForArtikelEvent()));
        branchedArtikelEventMap.get("branch-smartphones").to("artikel-smartphones", Produced.with(Serdes.String(), SerdeFactory.serdeForArtikelEvent()));
        branchedArtikelEventMap.get("branch-fotografie").to("artikel-fotografie", Produced.with(Serdes.String(), SerdeFactory.serdeForArtikelEvent()));

    }
}
