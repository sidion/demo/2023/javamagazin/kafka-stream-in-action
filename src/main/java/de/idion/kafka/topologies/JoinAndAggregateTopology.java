package de.idion.kafka.topologies;

import de.idion.kafka.events.BestellungEvent;
import de.idion.kafka.events.KundeEvent;
import de.idion.kafka.serde.SerdeFactory;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.*;
import org.springframework.stereotype.Component;

@Component
public class JoinAndAggregateTopology implements ExampleTopology {

    @Override
    public void createTopology(final StreamsBuilder builder) {
        // Kunden in KTable einlesen
        final KTable<String, KundeEvent> kundenTable = builder.table("kunden",
                Consumed.with(Serdes.String(), SerdeFactory.serdeForKundeEvent()));

        // Bestellungen in Stream einlesen
        final KStream<String, BestellungEvent> bestellungenStream =
                builder.stream("bestellungen", Consumed.with(Serdes.String(), SerdeFactory.serdeForBestellungEvent()));

        // Bestellungen mit Key=Kunde versehen
        final KStream<String, BestellungEvent> bestellungenWithNewKey = bestellungenStream.selectKey((key, val) -> val.getKundenIdx());


        // Bestellungen gruppieren anhand KundenIdx
        final KGroupedStream<String, BestellungEvent> groupedOrderStream =
                bestellungenWithNewKey.groupByKey(Grouped.with(Serdes.String(), SerdeFactory.serdeForBestellungEvent()));

        // Bestellungen aggregieren
        final KTable<String, BestellungEvent> aggregatedBestellungenByKunde = groupedOrderStream.aggregate(() -> new BestellungEvent(),
                (key, value, bestellungEvent) -> {
                    bestellungEvent.setGesamtSumme(bestellungEvent.getGesamtSumme() + value.getGesamtSumme());
                    return bestellungEvent;
                },
                Materialized.with(Serdes.String(), SerdeFactory.serdeForBestellungEvent()));


        // Bestellungen mit Kunden Joinen
        final ValueJoiner<BestellungEvent, KundeEvent, KundeEvent> valueJoiner = new ValueJoiner<>() {
            @Override
            public KundeEvent apply(BestellungEvent bestellungEvent, KundeEvent kundeEvent) {
                kundeEvent.setGesammtBestellSumme(bestellungEvent.getGesamtSumme());
                return kundeEvent;
            }
        };
        final KStream<String, KundeEvent> joinedBestellungen = aggregatedBestellungenByKunde.toStream().join(kundenTable, valueJoiner,
                Joined.with(Serdes.String(), SerdeFactory.serdeForBestellungEvent(), SerdeFactory.serdeForKundeEvent()));

        // Ausgabe auf Topic
        joinedBestellungen.to("bestellungen-aggregiert-pro-kunde", Produced.with(Serdes.String(), SerdeFactory.serdeForKundeEvent()));


    }
}
