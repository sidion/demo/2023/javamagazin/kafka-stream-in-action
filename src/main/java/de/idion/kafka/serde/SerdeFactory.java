package de.idion.kafka.serde;

import de.idion.kafka.events.ArtikelEvent;
import de.idion.kafka.events.BestellungEvent;
import de.idion.kafka.events.KundeEvent;
import de.idion.kafka.events.LagerEvent;
import org.apache.kafka.common.serialization.Serde;

public class SerdeFactory {

    public static Serde<KundeEvent> serdeForKundeEvent() {
        final CustomJsonSerde<KundeEvent> orderEventCustomJsonSerde = new CustomJsonSerde<>(KundeEvent.class);
        return orderEventCustomJsonSerde.get();
    }

    public static Serde<ArtikelEvent> serdeForArtikelEvent() {
        final CustomJsonSerde<ArtikelEvent> orderEventCustomJsonSerde = new CustomJsonSerde<>(ArtikelEvent.class);
        return orderEventCustomJsonSerde.get();
    }

    public static Serde<BestellungEvent> serdeForBestellungEvent() {
        final CustomJsonSerde<BestellungEvent> orderEventCustomJsonSerde = new CustomJsonSerde<>(BestellungEvent.class);
        return orderEventCustomJsonSerde.get();
    }

    public static Serde<LagerEvent> serdeForLagerEvent() {
        final CustomJsonSerde<LagerEvent> orderEventCustomJsonSerde = new CustomJsonSerde<>(LagerEvent.class);
        return orderEventCustomJsonSerde.get();
    }


}
