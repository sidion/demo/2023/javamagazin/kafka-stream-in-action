# Kafka Streams In Action

Demo Projekt für den Java-Magazin-Artikel 'Event-Verarbeitung mit Kafka Streams statt Consumer/Producer API' aus Heft 02.2024

Link zur Ausgabe: https://entwickler.de/magazine-ebooks/java-magazin/java-magazin-22024


### Kafka
Im Verzeichnis docker liegt ein docker-compose.yml mit dem ein Apache Kafka gestartet werden kann
Port: *9092*

### AKHQ
Im Verzeichnis akhq kann mit folgendem Befehl eine AKHQ gestartet werden:

`java -Dmicronaut.config.files=./application.yml -jar akhq.jar `


AKHQ: *http://localhost:8082/*


